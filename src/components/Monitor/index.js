import React from "react";
import monitors from "../../system/monitors.json";
import sshExec from "../helpers/sshExec";
import "./style.css";

const Monitor = (props) => {
  const { handlerLoad, sshConnectData, handlerAlertShow } = props;

  const handlerData = (data) => {
    handlerAlertShow(data);
  };

  const onClickHandler = (command) => {
    sshExec({
      handlerLoad,
      sshConnectData,
      handlerData,
      command,
    });
  };

  const listMonitors = monitors.map(({ id, title, command }) => (
    <input
      key={id}
      onClick={() => onClickHandler(command)}
      className="reload-btn btn"
      type="button"
      value={title}
    />
  ));

  return (
    <div className="monitor nav">
      <h2>Monitor</h2>
      {listMonitors}
    </div>
  );
};

export default Monitor;
