import React from "react";
import sshExec from "../helpers/sshExec";
import loadLogs from "../helpers/loadLogs";
import { SERVER_APP_PATH } from "../../system/constants";
import "./style.css";

const Service = (props) => {
  const {
    handlerLoad,
    handlerClose,
    handlerProcess,
    sshConnectData,
    uploadLogsDir,
  } = props;

  const clearLogsHandler = () => {
    const { domen } = sshConnectData;
    const command = `cd ${SERVER_APP_PATH}/nginxServer && rm -rf logs && mkdir logs && touch logs/${domen}.access.log && touch logs/${domen}.error.log  && touch logs/error.log && touch logs/access.log && cd ${SERVER_APP_PATH} && docker-compose restart nginx && docker-compose restart node`;

    sshExec({
      handlerLoad,
      handlerClose,
      sshConnectData,
      handlerProcess,
      command,
    });
  };

  const fullReloadSeverHandler = () => {
    const command = `cd ${SERVER_APP_PATH} && sh clear.all.sh && sh prod.run.sh`;

    sshExec({
      handlerLoad,
      handlerClose,
      sshConnectData,
      handlerProcess,
      command,
    });
  };

  const enableCache = () => {
    const command = `cd ${SERVER_APP_PATH} && sh redis.manager.sh enable-cache`;

    sshExec({
      handlerLoad,
      handlerClose,
      sshConnectData,
      handlerProcess,
      command,
    });
  };

  const disableCache = () => {
    const command = `cd ${SERVER_APP_PATH} && sh redis.manager.sh disable-cache`;

    sshExec({
      handlerLoad,
      handlerClose,
      sshConnectData,
      handlerProcess,
      command,
    });
  };

  const quickReloadSeverHandler = () => {
    const command = `cd ${SERVER_APP_PATH} && docker-compose restart nginx && docker-compose restart node && docker-compose restart redis`;

    sshExec({
      handlerLoad,
      handlerClose,
      sshConnectData,
      handlerProcess,
      command,
    });
  };

  const loadLogsHandler = () => {
    loadLogs({
      handlerLoad,
      handlerClose,
      uploadLogsDir,
      sshConnectData,
    });
  };

  return (
    <div className="service nav">
      <h2>Service</h2>
      <input
        onClick={loadLogsHandler}
        className="load-btn btn"
        type="button"
        value="Load logs"
      />
      <input
        onClick={clearLogsHandler}
        className="load-btn btn"
        type="button"
        value="Clear logs"
      />
      <input
        onClick={quickReloadSeverHandler}
        className="quick-reload-btn btn"
        type="button"
        value="Quick reload server"
      />
      <input
        onClick={fullReloadSeverHandler}
        className="full-reload-btn btn"
        type="button"
        value="Full reload server"
      />
      <input
        onClick={enableCache}
        className="full-reload-btn btn"
        type="button"
        value="Enable cache"
      />
      <input
        onClick={disableCache}
        className="full-reload-btn btn"
        type="button"
        value="Disable cache"
      />
    </div>
  );
};

export default Service;
