import React from "react";

const Settings = ({ onChangeData, fields }) =>
  fields.map(({ id, type, className, name, value, readOnly }) => {
    return (
      <input
        key={id}
        name={name}
        type={type}
        value={value}
        onChange={onChangeData}
        className={className}
        readOnly={readOnly}
      />
    );
  });

export default Settings;
