import fs from "fs";
import { SERVER_APP_PATH } from "../../system/constants";
import deleteFolder from "./deleteFolder";

const Client = require("ssh2").Client;

const loadLogs = ({
  handlerLoad,
  uploadLogsDir,
  sshConnectData,
  handlerClose,
}) => {
  const { host, username, password, domen } = sshConnectData;
  const isRun = confirm("do you want running this operation ?");
  if (!isRun) return null;
  const conn = new Client();

  handlerLoad(true);

  conn
    .on("ready", function () {
      conn.sftp(function (err, sftp) {
        if (err) throw err;

        deleteFolder(uploadLogsDir);

        fs.mkdirSync(uploadLogsDir);
        const moveAccessSemaFrom = `${SERVER_APP_PATH}/nginxServer/logs/${domen}.access.log`;
        const moveErrorSemaFrom = `${SERVER_APP_PATH}/nginxServer/logs/${domen}.error.log`;
        const moveAccessSemaTo = `${uploadLogsDir}/${domen}.access.log`;
        const moveeErrorSemaTo = `${uploadLogsDir}/${domen}.error.log`;

        const moveAccessFrom = `${SERVER_APP_PATH}/nginxServer/logs/access.log`;
        const moveErrorFrom = `${SERVER_APP_PATH}/nginxServer/logs/error.log`;
        const moveAccessTo = `${uploadLogsDir}/access.log`;
        const moveeErrorTo = `${uploadLogsDir}/error.log`;

        Promise.resolve()
          .then(() => {
            return sftp.fastGet(
              moveAccessSemaFrom,
              moveAccessSemaTo,
              {},
              function (downloadError) {
                if (downloadError) throw downloadError;
                console.log(`Succesfully uploaded ${domen}.access log`);
              }
            );
          })
          .then(() => {
            return sftp.fastGet(
              moveErrorSemaFrom,
              moveeErrorSemaTo,
              {},
              function (downloadError) {
                if (downloadError) throw downloadError;
                console.log(`Succesfully uploaded ${domen}.error log`);
              }
            );
          })
          .then(() => {
            return sftp.fastGet(moveAccessFrom, moveAccessTo, {}, function (
              downloadError
            ) {
              if (downloadError) throw downloadError;
              console.log("Succesfully uploaded error log");
            });
          })
          .then(() => {
            return sftp.fastGet(moveErrorFrom, moveeErrorTo, {}, function (
              downloadError
            ) {
              if (downloadError) throw downloadError;
            });
          })
          .then(() => {
            setTimeout(() => {
              handlerLoad(false);
            }, 1000);
            setTimeout(() => {
              handlerClose();
            }, 1200);
            console.log("Succesfully uploaded error log");
          });
      });
    })
    .connect({
      host,
      username,
      password,
    });
};

export default loadLogs;
