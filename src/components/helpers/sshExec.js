const Client = require("ssh2").Client;
const fs = require("fs");

const sshExec = ({
  handlerData,
  handlerClose,
  handlerLoad,
  handlerProcess,
  command,
  sshConnectData,
}) => {
  const isRun = confirm("do you want running this operation ?");

  const { host, username, password, port } = sshConnectData;

  if (!isRun) return null;

  console.log(`run command: ${command}`);

  handlerLoad(true);

  const conn = new Client();

  console.log(host, username, port);

  conn
    .on("ready", function () {
      console.log("Client :: ready");
      conn.exec(command, function (err, stream) {
        if (err) throw err;
        stream
          .on("close", function (code, signal) {
            console.log("ON CLOSE");
            console.log(
              "Stream :: close :: code: " + code + ", signal: " + signal
            );

            handlerLoad(false);

            if (handlerClose) {
              setTimeout(() => {
                handlerClose(code, signal);
              }, 500);
            }

            conn.end();
          })
          .on("data", function (data) {
            console.log("ON DADTA");
            handlerLoad(true);
            if (handlerData) {
              setTimeout(() => {
                handlerData(data);
              }, 500);
            }
          })
          .stderr.on("data", function (data) {
            console.log("ON DADTA STD");
            handlerLoad(true);
            if (data) {
              if (handlerProcess) {
                handlerProcess(data);
              }
            }
            console.log("STDERR: " + data);
          });
      });
    })
    .connect({
      host,
      username,
      port,
      //password,
      privateKey: fs.readFileSync("/home/alex/.ssh/id_rsa"),
    });
};

export default sshExec;
