import React from "react";
import Input from "../Input";

const Configs = ({ configurations, onClick }) =>
  configurations.map((configuration) => {
    const { id, domen, username, host, port, password } = configuration;
    return (
      <div key={id} className="info-row">
        <span>{domen}</span>
        <span>{username}</span>
        <span>{host}</span>
        <span>{port}</span>
        <Input value={password} />
        <input
          onClick={() => onClick(configuration)}
          type="button"
          value="check"
        />
      </div>
    );
  });

export default Configs;
