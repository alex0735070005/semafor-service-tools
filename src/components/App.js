import React, { useState } from "react";
import Spinner from "./Spinner";
import Alert from "./Alert";
import Settings from "./Settings";
import Configs from "./Configs";
import Monitor from "./Monitor";
import fields from "../system/fields.json";
import configurations from "../system/configurations";

import "./App.css";

const defaultConfiguration = configurations[0];

const App = () => {
  const [state, setState] = useState({
    ...defaultConfiguration,
    isLoad: false,
    dataAlert: null,
    isAlertShow: false,
    processString: "",
    fieldsData: fields.map((field) => ({
      ...field,
      value: defaultConfiguration[field.name],
    })),
  });

  const handlerLoad = (status) => {
    console.log("*********************");
    console.log(status);
    setState({
      ...state,
      isLoad: status,
    });
  };

  const handlerClose = (data) => {
    alert("OPERATION SUCCESS");
  };

  const handlerAlertClose = () => {
    setState({
      ...state,
      isAlertShow: false,
    });
  };

  const handlerAlertShow = (data) => {
    setState({
      ...state,
      isAlertShow: true,
      dataAlert: data,
    });
  };

  const handlerProcess = (data) => {
    setState({
      ...state,
      isLoad: true,
      processString: data.toString("utf8"),
    });
  };

  const onChangeData = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    setState({
      ...state,
      [name]: value,
      fieldsData: state.fieldsData.map((f) => {
        if (f.name === name) {
          return { ...f, value };
        }
        return f;
      }),
    });
  };

  const onChangeConfiguration = (configuration) => {
    alert("Do you want change config host ?");
    setState({
      ...state,
      ...configuration,
      fieldsData: fields.map((field) => ({
        ...field,
        value: configuration[field.name],
      })),
    });
  };

  const {
    isLoad,
    processString,
    fieldsData,
    host,
    port,
    username,
    password,
    domen,
    uploadLogsDir,
    dataAlert,
    isAlertShow,
  } = state;

  const sshConnectData = {
    host,
    username,
    password,
    domen,
    port,
  };

  return (
    <div>
      <h1>SSH MANAGER TOOLS</h1>

      <Configs
        configurations={configurations}
        onClick={onChangeConfiguration}
      />
      <div className="data-server">
        <Settings onChangeData={onChangeData} fields={fieldsData} />
      </div>

      <Monitor
        sshConnectData={sshConnectData}
        handlerLoad={handlerLoad}
        handlerAlertShow={handlerAlertShow}
      />

      <div className="proccess-string">{processString}</div>
      <Spinner isLoad={isLoad} />
      <Alert
        isShow={isAlertShow}
        data={dataAlert}
        onClose={handlerAlertClose}
      />
    </div>
  );
};

export default App;
