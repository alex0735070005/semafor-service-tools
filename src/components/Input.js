import React, { useState } from "react";

const Input = (props) => {
  const { value } = props;
  const [type, setType] = useState("password");

  const onShowPassword = () => {
    setType("text");
    setTimeout(() => setType("password"), 2000);
  };

  return (
    <div className="input-password">
      <input type={type} defaultValue={value} />
      <span onClick={onShowPassword}>Show password</span>
    </div>
  );
};

export default Input;
