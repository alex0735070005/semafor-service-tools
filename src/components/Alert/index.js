import React from "react";
import monitors from "../../system/monitors.json";
import sshExec from "../helpers/sshExec";
import "./style.css";

const Alert = (props) => {
  const { isShow, onClose, data } = props;

  if (!isShow) {
    return null;
  }

  const dataRender = data.toString("utf8").replace(/([0-9,.]+)/g, "<b>$1</b>");

  return (
    <div className="alert-wrap">
      <div className="alert">
        <input
          className="alert-close"
          onClick={onClose}
          type="button"
          value="close"
        />
        <pre dangerouslySetInnerHTML={{ __html: dataRender }} />
      </div>
    </div>
  );
};

export default Alert;
