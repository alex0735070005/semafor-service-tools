const downloadsFolder = require("downloads-folder");
const LOAD_DIR = downloadsFolder();

const configurations = [
  {
    id: "0",
    domen: "159.89.0.249",
    username: "pampik_api",
    host: "159.89.0.249",
    port: 22044,
    uploadLogsDir: "",
    password: "",
  },
  {
    id: "1",
    domen: "164.92.179.96",
    username: "root",
    host: "164.92.179.96",
    port: 22,
    uploadLogsDir: "",
    password: "",
  },
];

export default configurations;
